#include <ArduinoJson.h>

#define echoPin 4 // attach pin D2 Arduino to pin Echo of HC-SR04
#define trigPin 5 //attach pin D3 Arduino to pin Trig of HC-SR04

long duration; // variable for the duration of sound wave travel
int distance; // variable for the distance measurement
const int bufferSize = 100;
int circularBufferDistance[bufferSize];
int currentIndex = 0;
String message = "";
bool messageReady = false;
float full = 9;
float empty = 129;
float totalLitres = 4000;
void setup()
{
  //ESP basic communication via serial
  Serial.begin(9600); // communication with the host computer

  //ultra sonic sensor initialization
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin, INPUT); // Sets the echoPin as an INPUT
}

void loop()
{
  // ultrasonic sensor
  // Clears the trigPin condition
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin HIGH (ACTIVE) for 10 microseconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  duration = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance = duration * 0.034 / 2; // Speed of sound wave divided by 2 (go and back)

  //buffer distance values into circularbuffer
  circularBufferDistance[currentIndex] = distance;

  if (currentIndex < bufferSize - 1) {
    currentIndex += 1;
  }
  else {
    currentIndex = 0;
  }

  //ESP webserver communication
  // Monitor serial communication
  while (Serial.available()) {
    message = Serial.readString();
    messageReady = true;
  }
  // Only process message if there's one
  if (messageReady) {
    // The only messages we'll parse will be formatted in JSON
    DynamicJsonDocument doc(1024); // ArduinoJson version 6+
    // Attempt to deserialize the message
    DeserializationError error = deserializeJson(doc, message);
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.c_str());
      messageReady = false;
      return;
    }
    if (doc["type"] == "request") {
      doc["type"] = "response";
      //doc["distance"] = distance; //set calculated distance value in json
      float accumulatedDistance = 0;
      float averageDistance = 0;
      for (int i = 0; i < bufferSize; i++) {
        accumulatedDistance += (float)circularBufferDistance[i];
      }
      averageDistance = accumulatedDistance / bufferSize;
      doc["distance"] = averageDistance;
      float percentageFilled = 1.0 - ((float)distance - full) / (empty - full);
      doc["percentage"] = round(100 * percentageFilled);
      doc["availableLitres"] = round((float)totalLitres * percentageFilled);
      serializeJson(doc, Serial); //send response to serial
    }
    messageReady = false;
  }
}
