# Arduino Uno Ultrasonic Water Level Sensor

## uno/uno.ino
Used for Arduino Uno ATmega328P

### Arduino.app setup: 
- Tools --> Board --> Arduino Uno
- Tools --> Port --> usbserial-A60...

## esp/esp.ino
Used for ESP8266 (ESP-01) WiFi module

### Arduino.app setup: 
- Install ESP library
- Tools --> ESP8266 Boards --> Generic ESP8266 Module

Flash via USB Adapter (needs to be in DLOAD for development/flashing, UART mode for production). After changing the mode, the ESP01 needs to be reconnected manually.

## Troubleshooting

### stk500_recv() programmer is not responding

Make sure the pins 0 and 1 (TX & RX) are not connected