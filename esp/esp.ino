#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>

ESP8266WebServer server;
char* ssid = "FRITZ!Box 5490 SG"; 
char* password = "24747094074388506888";

void setup()
{
  //Static IP address configuration
  IPAddress staticIP(192, 168, 178, 53); //ESP static ip
  IPAddress gateway(192, 168, 178, 1);   //IP Address of your WiFi Router (Gateway)
  IPAddress subnet(255, 255, 255, 0);  //Subnet mask
  IPAddress dns(8, 8, 8, 8);  //DNS
  WiFi.config(staticIP, subnet, gateway, dns);

  WiFi.begin(ssid,password);
  Serial.begin(9600);
  while(WiFi.status()!=WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  server.on("/",handleIndex);
  server.begin();
  
}

void loop()
{
  server.handleClient();
}

void handleIndex()
{
  // Send a JSON-formatted request with key "type" and value "request"
  // then parse the JSON-formatted response with key "distance"
  DynamicJsonDocument doc(1024);
  float distance = 0;
  int percentage = 0;
  int availableLitres = 0;
  
  // Sending the request
  doc["type"] = "request";
  serializeJson(doc,Serial);  //send request to serial
  // Reading the response
  boolean messageReady = false;
  String message = "";
  while(messageReady == false) { // blocking but that's ok
    if(Serial.available()) {
      message = Serial.readString();  //read response from arduino
      messageReady = true;
    }
  }
  // Attempt to deserialize the JSON-formatted message (into doc)
  DeserializationError error = deserializeJson(doc,message);
  if(error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }
  distance = doc["distance"];
  percentage = doc["percentage"];
  availableLitres = doc["availableLitres"];
  
  // Prepare the data for serving it over HTTP
  String output = "{\n\"percentageFilled\": " + String(percentage) + ",\n" + "\"litresAvailable\": " + String(availableLitres) + ",\n" + "\"distance\": " + String(distance) + "\n}";
  server.send(200,"text/plain", output);
  Serial.readString();
}
